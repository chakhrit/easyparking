package com.chakhrit.eazyparking.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivity;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.fragment.MapFragment;
import com.chakhrit.eazyparking.fragment.SearchFragment;
import com.chakhrit.eazyparking.fragment.UserInformationFragment;
import com.chakhrit.eazyparking.model.Devices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AbstractActivity implements BottomNavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemReselectedListener {

    private BottomNavigationView bottomBar;
    private Intent intent;
    Activity activity;

    @Override
    protected int setContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected int bindActionBar() {
        menuTitleAndRight("Home", R.drawable.ic_refresh_white_24dp);
        return MENU_S;
    }

    @Override
    protected void bindUI() {
        bottomBar = findViewById(R.id.bottomBar);
    }

    @Override
    protected void setupUI() {
        setFirstPage();
        bottomBar.setOnNavigationItemSelectedListener(this);
        bottomBar.setOnNavigationItemReselectedListener(this);

    }

    private void setFirstPage() {
        bottomBar.setSelectedItemId(R.id.item_map);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            intent = new Intent(getApplicationContext(), SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            switchContent(new MapFragment(), null);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_map:
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    switchContent(new MapFragment(), null);
                }
                break;
            case R.id.item_search:
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    switchContent(new SearchFragment(), null);
                }
                break;
            case R.id.item_user:
                if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                    intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    switchContent(new UserInformationFragment(), null);
                }
                break;

        }
        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    public void switchContent(Fragment fragment, Bundle bundle) {

        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }

        fragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left)
                .replace(R.id.contentMain, fragment)
                .commit();
    }

}
