package com.chakhrit.eazyparking.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivity;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.SharedPrefs;
import com.chakhrit.eazyparking.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class RegisterActivity extends AbstractActivity implements View.OnClickListener, TextView.OnEditorActionListener {

    private static final int REQUEST_LOCATION = 0;
    private TextView tvRegister;
    private EditText edtEmail, edtPassword, edtConfirmPassword, edtFirstName, edtLastName, edtTel;
    private Intent intent;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private StorageReference mStorageRef;

    private Uri uri;

    @Override
    protected int setContentView() {
        return R.layout.activity_register;
    }

    @Override
    protected int bindActionBar() {
        menuTitleAndLeft(R.drawable.ic_keyboard_backspace_white_24dp, getString(R.string.txt_register));
        return MENU_S;
    }

    @Override
    protected void bindUI() {
        tvRegister = findViewById(R.id.tvRegister);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtFirstName = findViewById(R.id.edtFirstName);
        edtLastName = findViewById(R.id.edtLastName);
        edtTel = findViewById(R.id.edtTel);
    }

    @Override
    protected void setupUI() {
        tvRegister.setOnClickListener(this);
        ivLeft.setOnClickListener(this);

        edtTel.setOnEditorActionListener(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRegister:
                createUser();
                break;
            case R.id.ivLeft:
                onBackPressed();
                break;
        }
    }

    private void createUser() {
        if (!validateForm()) {
            return;
        }
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(edtEmail.getText().toString(), edtPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            onAuthSuccess(task.getResult().getUser());
                        } else {
                            Functions.setAlertDialogWithOk(task.getException().getMessage().toString(), RegisterActivity.this);
                            Log.w("Firebase", "createUserWithEmail:failure", task.getException());
                        }
                        progressDialog.cancel();
                    }
                });
    }

    private void onAuthSuccess(FirebaseUser user) {
        uri = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.drawable.icon_user);
        //uploadProfileImg(user, uri);

        checkPermission();
        writeNewUser(user.getUid(), user.getEmail(), edtFirstName.getText().toString(),
                edtLastName.getText().toString(), edtTel.getText().toString());
    }

    private void uploadProfileImg(final FirebaseUser user, final Uri uri) {
        mStorageRef = FirebaseStorage.getInstance().getReference("profileImage/" + user.getUid());
        mStorageRef.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d("UploadProfileRegister", "Upload Image success");
                Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();

                writeNewUser(user.getUid(), user.getEmail(), edtFirstName.getText().toString(),
                        edtLastName.getText().toString(), edtTel.getText().toString());
            }
        });
    }

    private void writeNewUser(String uid, String email, String firstName, String lastName, String tel) {
        User user = new User(email, firstName, lastName, tel);

        mDatabase.child("users").child(uid).setValue(user);
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            gotoMain();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gotoMain();
            } else {
                checkPermission();
            }
        }
    }

    private void gotoMain() {
        SharedPrefs.getInstance(this).setLogin(true);
        intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private boolean validateForm() {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            edtEmail.setError(getString(R.string.txt_required));
            return false;
        } else if (TextUtils.isEmpty(edtPassword.getText().toString().trim())) {
            edtPassword.setError(getString(R.string.txt_required));
            return false;
        } else if (TextUtils.isEmpty(edtFirstName.getText().toString().trim())) {
            edtFirstName.setError(getString(R.string.txt_required));
            return false;
        } else if (TextUtils.isEmpty(edtLastName.getText().toString().trim())) {
            edtLastName.setError(getString(R.string.txt_required));
            return false;
        } else if (TextUtils.isEmpty(edtTel.getText().toString().trim()) || edtTel.getText().toString().trim().length() < 10) {
            edtTel.setError(getString(R.string.txt_required));
            return false;
        } else if (!edtEmail.getText().toString().trim().matches(emailPattern)) {
            edtEmail.setError(getString(R.string.txt_invalid_email));
            return false;
        } else if (!edtPassword.getText().toString().trim().equals(edtConfirmPassword.getText().toString().trim())) {
            edtPassword.setError(getString(R.string.txt_pass_not_match));
            edtConfirmPassword.setError(getString(R.string.txt_pass_not_match));
            return false;
        } else {
            edtEmail.setError(null);
            edtPassword.setError(null);
            edtConfirmPassword.setError(null);
            edtFirstName.setError(null);
            edtLastName.setError(null);
            edtTel.setError(null);
            return true;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            createUser();
            return true;
        }
        return false;
    }
}
