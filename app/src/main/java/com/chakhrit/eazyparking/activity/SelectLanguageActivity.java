package com.chakhrit.eazyparking.activity;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivityNoBar;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.SharedPrefs;

public class SelectLanguageActivity extends AbstractActivityNoBar implements View.OnClickListener {

    private TextView tvThaiLanguage;
    private TextView tvEngLanguage;
    private Intent intent;

    @Override
    protected int setContentView() {
        return R.layout.activity_select_language;
    }

    @Override
    protected void bindUI() {
        tvEngLanguage = findViewById(R.id.tvEngLanguage);
        tvThaiLanguage = findViewById(R.id.tvThaiLanguage);

    }

    @Override
    protected void setupUI() {
        tvEngLanguage.setOnClickListener(this);
        tvThaiLanguage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvEngLanguage:
                setLanguage("en");
                intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tvThaiLanguage:
                setLanguage("th");
                intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
