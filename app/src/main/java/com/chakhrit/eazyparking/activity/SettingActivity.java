package com.chakhrit.eazyparking.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivity;
import com.chakhrit.eazyparking.custom.SharedPrefs;
import com.chakhrit.eazyparking.fragment.UserInformationFragment;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingActivity extends AbstractActivity implements View.OnClickListener {

    private TextView tvVersion, tvLanguage;
    private ImageView ivFlag;
    private Button btnSignOut;
    private LinearLayout llLanguage;
    private Intent intent;

    @Override
    protected int setContentView() {
        return R.layout.activity_setting;
    }

    @Override
    protected int bindActionBar() {
        menuTitleAndLeft(R.drawable.ic_keyboard_backspace_white_24dp, getString(R.string.txt_setting));
        return MENU_S;
    }

    @Override
    protected void bindUI() {
        tvVersion = findViewById(R.id.tvVersion);
        tvLanguage = findViewById(R.id.tvLanguage);
        ivFlag = findViewById(R.id.ivFlag);
        btnSignOut = findViewById(R.id.btnSignOut);
        llLanguage = findViewById(R.id.llLanguage);
    }


    @Override
    protected void setupUI() {
        setVersion();
        checkLanguage();
        //setClickedLeft(this);
        llLanguage.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
        ivLeft.setOnClickListener(this);
    }

    private void setVersion() {
        String versionName = null;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        tvVersion.setText(versionName);
    }

    private void showLanguageDialog() {
        AlertDialog.Builder languageDialog = new AlertDialog.Builder(this);
        languageDialog.setTitle(R.string.txt_select_language);
        String[] languageDialogItems = {
                getResources().getString(R.string.select_language_txtThai),
                getResources().getString(R.string.select_language_txtEng)};
        languageDialog.setItems(languageDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                setLanguage("th");
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case 1:
                                setLanguage("en");
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                    }
                });
        languageDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.llLanguage:
                showLanguageDialog();
                break;
            case R.id.btnSignOut:
                SharedPrefs.getInstance(this).setLogin(false);
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(this, SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.ivLeft:
                finish();
                break;
        }
    }

    public void checkLanguage() {
        Locale currentLocale = Locale.getDefault();
        if (currentLocale.getLanguage().toString().equals("th")) {
            tvLanguage.setText(getResources().getText(R.string.select_language_txtThai));
            ivFlag.setImageResource(R.drawable.ic_thailand);
        } else {
            tvLanguage.setText(getResources().getText(R.string.select_language_txtEng));
            ivFlag.setImageResource(R.drawable.ic_united_kingdom);
        }
    }

}
