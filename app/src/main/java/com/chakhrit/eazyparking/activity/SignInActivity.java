package com.chakhrit.eazyparking.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivityNoBar;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.SharedPrefs;
import com.chakhrit.eazyparking.dialog.ForgotPasswordDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AbstractActivityNoBar implements View.OnClickListener, TextView.OnEditorActionListener {

    private static final int REQUEST_LOCATION = 0;
    private TextView tvSignUp, tvForgotPassword;
    private EditText edtEmail, edtPassword;
    private Button btnSignIn;
    private Intent intent;

    private FirebaseAuth mAuth;

    @Override
    protected int setContentView() {
        return R.layout.activity_sign_in;
    }

    @Override
    protected void bindUI() {
        tvSignUp = findViewById(R.id.tvSignUp);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
    }

    @Override
    protected void setupUI() {
        tvSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);

        edtPassword.setOnEditorActionListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSignUp:
                intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSignIn:
                firebaseAuthen();
                break;
            case R.id.tvForgotPassword:
                ForgotPasswordDialog forgotPasswordDialog = new ForgotPasswordDialog(this);
                forgotPasswordDialog.setCanceledOnTouchOutside(false);
                forgotPasswordDialog.show();
                break;
        }
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            gotoMain();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                gotoMain();
            } else {
                checkPermission();
            }
        }
    }

    private void gotoMain() {
        SharedPrefs.getInstance(this).setLogin(true);
        intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void firebaseAuthen() {
        if (!validateForm()) {
            return;
        }
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(edtEmail.getText().toString().trim(), edtPassword.getText().toString().trim())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            checkPermission();
                        } else {
                            Functions.setAlertDialogWithOk(getString(R.string.txt_can_not_log_in), SignInActivity.this);
                        }
                        progressDialog.cancel();
                    }
                });
    }

    private boolean validateForm() {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            edtEmail.setError(getString(R.string.txt_required));
            return false;
        } else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            edtPassword.setError(getString(R.string.txt_required));
            return false;
        } else if (!edtEmail.getText().toString().matches(emailPattern)) {
            edtEmail.setError(getString(R.string.txt_invalid_email));
            return false;
        } else {
            edtEmail.setError(null);
            edtPassword.setError(null);
            return true;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            firebaseAuthen();
            return true;
        }
        return false;
    }
}
