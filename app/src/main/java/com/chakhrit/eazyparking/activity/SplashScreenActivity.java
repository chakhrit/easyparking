package com.chakhrit.eazyparking.activity;

import android.content.Intent;
import android.os.Handler;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.AbstractActivityNoBar;
import com.chakhrit.eazyparking.custom.SharedPrefs;

public class SplashScreenActivity extends AbstractActivityNoBar {

    private static final int SPLASH_DELAY = 3000;

    private final Handler mHandler   = new Handler();
    private final Launcher mLauncher = new Launcher();

    @Override
    protected int setContentView() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void bindUI() {

    }

    @Override
    protected void setupUI() {

    }

    @Override
    protected void onStart() {
        super.onStart();

        mHandler.postDelayed(mLauncher, SPLASH_DELAY);
    }

    @Override
    protected void onStop() {
        mHandler.removeCallbacks(mLauncher);
        super.onStop();
    }


    private class Launcher implements Runnable {
        @Override
        public void run() {
            launch();
        }
    }

    private void launch() {
        if (!isFinishing()) {
            checkLogin();
        }
    }

    private void checkLogin() {
        if (SharedPrefs.getInstance(this).isLogin() == true) {
            startActivity(new Intent(this, MainActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }else {
            startActivity(new Intent(this, SelectLanguageActivity.class));
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }
}
