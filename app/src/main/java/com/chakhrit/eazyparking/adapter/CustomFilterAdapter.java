package com.chakhrit.eazyparking.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.Fonts;

public class CustomFilterAdapter extends BaseAdapter{

    private Activity activity;
    private String[] sortBy;
    private LayoutInflater inflater;

    public CustomFilterAdapter(Activity activity, String[] sortBy) {
        this.activity = activity;
        this.sortBy = sortBy;
        inflater = (LayoutInflater.from(activity));
    }

    @Override
    public int getCount() {
        return sortBy.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.custom_spinner, null);
        TextView tvLocation = convertView.findViewById(R.id.tvLocation);
        tvLocation.setText(sortBy[position]);
        new Fonts(activity).setTypeface(tvLocation);
        return convertView;
    }
}
