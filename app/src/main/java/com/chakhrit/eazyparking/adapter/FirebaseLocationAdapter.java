package com.chakhrit.eazyparking.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.GPSTracker;
import com.chakhrit.eazyparking.model.Devices;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.Query;

public class FirebaseLocationAdapter extends FirebaseRecyclerAdapter<Devices, FirebaseLocationViewHolder> {

    private Activity activity;
    public FirebaseLocationAdapter.OnLocationClickListener onLocationClickListener;


    public FirebaseLocationAdapter(Class<Devices> modelClass, int modelLayout, Class<FirebaseLocationViewHolder> viewHolderClass, Query ref, Activity activity) {
        super(modelClass, modelLayout, viewHolderClass, ref);
        this.activity = activity;
    }

    @Override
    protected void populateViewHolder(FirebaseLocationViewHolder viewHolder, final Devices model, int position) {
        GPSTracker gpsTracker = new GPSTracker(activity);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(),"thaisansneue-regular.ttf");

        viewHolder.tvParkingLot.setTypeface(typeface);
        viewHolder.tvParkingTitle.setTypeface(typeface);
        viewHolder.tvLocationName.setTypeface(typeface);
        viewHolder.tvDistance.setTypeface(typeface);
        viewHolder.tvLocation.setTypeface(typeface);
        viewHolder.tvMetric.setTypeface(typeface);

        viewHolder.tvLocationName.setText(model.getLocation());
        viewHolder.tvParkingLot.setText(model.getName());
        if (model.isAvailable()) {
            viewHolder.ivColor.setBackgroundResource(R.color.green);
        } else {
            viewHolder.ivColor.setBackgroundResource(R.color.red);
        }

        if (model.isDisable()) {
            viewHolder.ivDisable.setImageResource(R.drawable.icon_disabled);
        } else {
            viewHolder.ivDisable.setVisibility(View.GONE);
        }

        Functions.glideImageCircleWithUrl(activity, model.getImage(), viewHolder.ivParking);
        LatLng defaultLocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        viewHolder.tvDistance.setText(String.valueOf(Functions.distance(defaultLocation.latitude,
                defaultLocation.longitude,
                model.getLat(),
                model.getLng())));

        viewHolder.llNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLocationClickListener.setOnLocationClick(model);
            }
        });
    }

    public interface OnLocationClickListener {
        void setOnLocationClick(Devices devices);
    }

    public void setOnLocationClickListener(FirebaseLocationAdapter.OnLocationClickListener onLocationClickListener) {
        this.onLocationClickListener = onLocationClickListener;
    }
}
