package com.chakhrit.eazyparking.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;

public class FirebaseLocationViewHolder extends RecyclerView.ViewHolder {

    public TextView tvLocationName, tvDistance,tvMetric, tvLocation, tvParkingLot, tvParkingTitle;
    public ImageView ivColor, ivParking, ivDisable;
    public LinearLayout llNavigation;

    public FirebaseLocationViewHolder(View itemView) {
        super(itemView);
        tvLocationName = itemView.findViewById(R.id.tvLocationName);
        tvParkingTitle = itemView.findViewById(R.id.tvParkingTitle);
        tvParkingLot = itemView.findViewById(R.id.tvParkingLot);
        tvDistance = itemView.findViewById(R.id.tvDistance);
        tvMetric = itemView.findViewById(R.id.tvMetric);
        tvLocation = itemView.findViewById(R.id.tvLocation);
        ivColor = itemView.findViewById(R.id.ivColor);
        ivParking = itemView.findViewById(R.id.ivParking);
        ivDisable = itemView.findViewById(R.id.ivDisable);
        llNavigation = itemView.findViewById(R.id.llNavigation);
    }
}
