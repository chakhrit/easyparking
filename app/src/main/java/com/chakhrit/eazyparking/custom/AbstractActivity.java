package com.chakhrit.eazyparking.custom;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.chakhrit.eazyparking.R;
import com.github.ybq.android.spinkit.style.DoubleBounce;

/**
 * Created by chakhrit on 13/1/2018 AD.
 */

public abstract class AbstractActivity extends LocalizationActivity {
    private int menuType = 0;
    public static final int MENU_S = 0;
    public static final int MENU_H = 1;
    private LinearLayout toolBar;
    protected ImageView ivLeft, ivRight;
    protected TextView tvTitle;
    protected ProgressDialog progressDialog;

    protected abstract int setContentView();

    protected abstract int bindActionBar();

    protected abstract void bindUI();

    protected abstract void setupUI();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentView());
        new Fonts(this).setTypeFaceAllTextView((ViewGroup) findViewById(android.R.id.content));
        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        setProgressDialog();
        defaultActionBar();
        bindUI();
        setupUI();

    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_loading));
        progressDialog.setCancelable(false);
    }

    private void defaultActionBar() {
        toolBar = findViewById(R.id.toolBar);
        tvTitle = findViewById(R.id.tvToolbar);
        ivLeft = findViewById(R.id.ivLeft);
        ivRight = findViewById(R.id.ivRight);

        switch (menuType) {
            case MENU_S:
                showActionBar();
                break;
            case MENU_H:
                hideActionBar();
                break;
        }
        menuType = bindActionBar();
    }

    public void hideActionBar() {
        toolBar.setVisibility(View.GONE);
    }

    public void showActionBar() {
        toolBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getRootView().getWindowToken(), 0);
        return true;
    }

    public void setClickedLeft(View.OnClickListener onClickListener) {
        ivLeft.setOnClickListener(onClickListener);
    }

    public void setClickedRight(View.OnClickListener onClickListener) {
        ivRight.setOnClickListener(onClickListener);
    }

    public void menuTitle(String title) {
        ivLeft.setVisibility(View.GONE);
        ivRight.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }
    public void menuTitleAndLeft(int imgLeft, String title){
        ivLeft.setVisibility(View.VISIBLE);
        ivLeft.setImageResource(imgLeft);
        ivRight.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }

    public void menuTitleAndRight(String title, int imgRight){
        ivLeft.setVisibility(View.GONE);
        ivRight.setVisibility(View.VISIBLE);
        ivRight.setImageResource(imgRight);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }

    public void fullMenu(int imgLeft, String title , int imgRight) {
        ivLeft.setVisibility(View.VISIBLE);
        ivLeft.setImageResource(imgLeft);
        ivRight.setVisibility(View.VISIBLE);
        ivRight.setImageResource(imgRight);
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText(title);
    }
}
