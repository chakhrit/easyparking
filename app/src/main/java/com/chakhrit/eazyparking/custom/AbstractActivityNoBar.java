package com.chakhrit.eazyparking.custom;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.chakhrit.eazyparking.R;

/**
 * Created by chakhrit on 13/1/2018 AD.
 */

public abstract class AbstractActivityNoBar extends LocalizationActivity {

    protected ProgressDialog progressDialog;

    protected abstract int setContentView();

    protected abstract void bindUI();

    protected abstract void setupUI();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setContentView());
        new Fonts(this).setTypeFaceAllTextView((ViewGroup) findViewById(android.R.id.content));
        this.overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

        setProgressDialog();
        bindUI();
        setupUI();
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.txt_loading));
        progressDialog.setCancelable(false);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getRootView().getWindowToken(), 0);
        return true;
    }
}
