package com.chakhrit.eazyparking.custom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chakhrit.eazyparking.R;

/**
 * Created by chakhrit on 13/1/2018 AD.
 */

public abstract class AbstractFragment extends Fragment {
    protected Activity activity;
    protected LayoutInflater layoutInflater;
    protected Bundle bundle;
    protected ProgressDialog progressDialog;

    protected abstract int setContentView();

    protected abstract void bindUI(View v);

    protected abstract void setupUI();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(setContentView(), container, false);
        this.bundle = savedInstanceState;
        setHasOptionsMenu(true);
        activity = getActivity();
        this.layoutInflater = inflater;
        new Fonts(activity).setTypeFaceAllTextView((ViewGroup) v);

        setProgressDialog();
        bindUI(v);
        setupUI();

        /*InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(v.getWindowToken(), 0);*/
        return v;
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(getString(R.string.txt_loading));
        progressDialog.setCancelable(false);
    }


    public void replaceFragment(Fragment fragment, Bundle bundle) {
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.contentMain, fragment).setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left)
                .addToBackStack(null)
                .commit();
    }
}
