package com.chakhrit.eazyparking.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by chakhrit on 11/1/2018 AD.
 */

public class Fonts {
    public Context context;

    public Fonts(Context context) {
        this.context = context;
    }

    public void setTypeFaceAllTextView(ViewGroup viewGroup) {
        if (viewGroup.getChildCount() > 0) {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (viewGroup.getChildAt(i) instanceof TextView) {
                    setTypeface((TextView) viewGroup.getChildAt(i));
                } else if (viewGroup.getChildAt(i) instanceof ViewGroup) {
                    setTypeFaceAllTextView((ViewGroup) viewGroup.getChildAt(i));
                }
            }
        }
    }

    public void setTypeface(TextView textView) {
        if (textView != null) {
            if (textView.getTypeface() != null && textView.getTypeface().isBold()) {
                textView.setTypeface(getBoldFont());
            } else {
                textView.setTypeface(getNormalFont());
            }
        }
    }

    private Typeface getNormalFont() {
        Typeface normalFonts = null;
        if (normalFonts == null) {
            normalFonts = Typeface.createFromAsset(context.getAssets(), "thaisansneue-regular.ttf");
        }
        return normalFonts;
    }

    private Typeface getBoldFont() {
        Typeface blodFonts = null;
        if (blodFonts == null) {
            blodFonts = Typeface.createFromAsset(context.getAssets(), "thaisansneue-semibold.ttf");
        }
        return blodFonts;
    }
}
