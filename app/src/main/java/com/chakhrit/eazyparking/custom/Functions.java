package com.chakhrit.eazyparking.custom;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.chakhrit.eazyparking.R;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by chakhrit on 21/1/2018 AD.
 */

public class Functions {

    public static void setAlertDialogWithOk(String message, Activity activity) {
        MaterialDialog dialog = new MaterialDialog.Builder(activity)
                .title(R.string.app_name)
                .content(message)
                .positiveText(R.string.txt_confirm)
                .show();
    }

    public static void glideImageCircle(final Activity activity, int resId, final ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.text_app)
                .error(R.drawable.text_app);

        Glide.with(activity)
                .load(resId)
                .apply(options)
                .into(imageView);
    }

    public static void glideImageCenterCropWithUrl(final Activity activity, String imgUrl, final ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.text_app)
                .error(R.drawable.text_app);

        Glide.with(activity)
                .load(imgUrl)
                .apply(options)
                .into(imageView);
    }

    public static void updateLanguage(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }

    public static void glideImageCircleWithUrl(final Activity activity, String url, final ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .circleCrop()
                .placeholder(R.drawable.text_app)
                .error(R.drawable.text_app);

        Glide.with(activity)
                .load(url)
                .apply(options)
                .into(imageView);
    }

    public static float distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(Math.toRadians(lat_a)) * Math.cos(Math.toRadians(lat_b)) *
                        Math.sin(lngDiff / 2) * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        double kiloMeterConversion = 1.609344;
        DecimalFormat formater = new DecimalFormat("#.##");
        float dis = Float.parseFloat(formater.format(new Float(distance * kiloMeterConversion).floatValue()));

        return dis;
    }

}
