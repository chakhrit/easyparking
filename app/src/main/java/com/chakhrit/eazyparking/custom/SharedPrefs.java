package com.chakhrit.eazyparking.custom;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by chakhrit on 11/1/2018 AD.
 */

public class SharedPrefs {

    public static final int THAI = 1;
    public static final int ENG = 2;
    private static SharedPrefs sharedPrefs;
    private Context context;
    private SharedPreferences preferencesData;

    private SharedPrefs(Context context) {
        if (context instanceof Application) {
            this.context = context;
        } else {
            this.context = context.getApplicationContext();
        }
        preferencesData = context.getSharedPreferences("mySharedPrefs", Context.MODE_PRIVATE);
    }

    public static SharedPrefs getInstance(Context context) {
        if (sharedPrefs == null) {
            sharedPrefs = new SharedPrefs(context);
        }
        return sharedPrefs;
    }

    public void setLanguage(int language) {
        SharedPreferences.Editor editor = preferencesData.edit();
        editor.putInt("Language", language);
        editor.apply();
    }

    public int getLanguage() {
        return preferencesData.getInt("Language", 1);
    }

    public void setLogin(boolean login) {
        SharedPreferences.Editor editor = preferencesData.edit();
        editor.putBoolean("isLogin", login);
        editor.apply();
    }

    public boolean isLogin() {
        return preferencesData.getBoolean("isLogin", false);
    }
}
