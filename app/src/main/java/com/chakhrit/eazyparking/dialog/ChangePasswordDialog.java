package com.chakhrit.eazyparking.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.Fonts;
import com.chakhrit.eazyparking.custom.Functions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordDialog extends Dialog implements View.OnClickListener {

    private Activity activity;
    private TextView btnCancel, btnConfirm;
    private EditText edtNewPassword, edtConfirmPassword;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();;
    private FirebaseUser user = mAuth.getCurrentUser();

    public ChangePasswordDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.change_password_dialog);
        new Fonts(activity).setTypeFaceAllTextView((ViewGroup) findViewById(android.R.id.content));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        bindUI();
        setupUI();
    }

    private void bindUI() {
        btnCancel = findViewById(R.id.btnCancel);
        btnConfirm = findViewById(R.id.btnConfirm);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
    }

    private void setupUI() {
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnConfirm:
                if (!validatePassword()) {
                    return;
                }
                updatePassword(user);
                break;
        }
    }

    private void updatePassword(FirebaseUser user) {
        user.updatePassword(edtNewPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Functions.setAlertDialogWithOk(activity.getString(R.string.txt_complete), activity);
                    dismiss();
                } else {
                    Functions.setAlertDialogWithOk(task.getException().getMessage().toString(), activity);
                    dismiss();
                }
            }
        });
    }

    private boolean validatePassword() {
        String newPassword = edtNewPassword.getText().toString();
        String confirmPassword = edtConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(newPassword) || TextUtils.isEmpty(confirmPassword)) {
            edtNewPassword.setError(activity.getString(R.string.txt_required));
            edtConfirmPassword.setError(activity.getString(R.string.txt_required));
            return false;
        } else if (!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            edtNewPassword.setError(activity.getString(R.string.txt_pass_not_match));
            edtConfirmPassword.setError(activity.getString(R.string.txt_pass_not_match));
            return false;
        } else {
            edtNewPassword.setError(null);
            edtConfirmPassword.setError(null);
            return true;
        }
    }
}
