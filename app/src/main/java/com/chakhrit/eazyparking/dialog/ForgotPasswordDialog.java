package com.chakhrit.eazyparking.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.Fonts;
import com.chakhrit.eazyparking.custom.Functions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordDialog extends Dialog implements View.OnClickListener {

    private Activity activity;
    private Button btnCancel, btnConfirm;
    private EditText edtEmail;


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public ForgotPasswordDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.forgot_password_dialog);
        new Fonts(activity).setTypeFaceAllTextView((ViewGroup) findViewById(android.R.id.content));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        bindUI();
        setupUI();
    }

    private void bindUI() {
        btnCancel = findViewById(R.id.btnCancel);
        btnConfirm = findViewById(R.id.btnConfirm);
        edtEmail = findViewById(R.id.edtEmail);
    }

    private void setupUI() {
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnConfirm:
                if (!validateEmail()) {
                    return;
                }
                resetPasswordToEmail();
                break;
        }
    }

    private void resetPasswordToEmail() {
        mAuth.sendPasswordResetEmail(edtEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Functions.setAlertDialogWithOk(activity.getString(R.string.txt_complete), activity);
                    dismiss();
                }else {
                    Functions.setAlertDialogWithOk(task.getException().getMessage().toString(), activity);
                    dismiss();
                }
            }
        });
    }

    private boolean validateEmail() {
        String email = edtEmail.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(email)) {
            edtEmail.setError(activity.getString(R.string.txt_required));
            return false;
        } else if (!edtEmail.getText().toString().matches(emailPattern)) {
            edtEmail.setError(activity.getString(R.string.txt_invalid_email));
            return false;
        }else {
            edtEmail.setError(null);
            return true;
        }
    }
}
