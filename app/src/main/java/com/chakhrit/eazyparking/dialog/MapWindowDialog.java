package com.chakhrit.eazyparking.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.custom.Fonts;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.GPSTracker;
import com.chakhrit.eazyparking.model.Devices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by chakhrit on 25/2/2018 AD.
 */

public class MapWindowDialog extends Dialog implements View.OnClickListener {

    private Activity activity;
    private Devices devices;

    private TextView tvLocationName, tvDistance, tvParkingLot;
    private ImageView ivParking, ivDisable;
    private Button btnNavigation;

    private Double latitude, longitude;

    private LatLng defaultLocation;
    private GPSTracker gpsTracker;

    public MapWindowDialog(Activity activity, Devices devices, Double latitude, Double longitude) {
        super(activity);
        this.activity = activity;
        this.devices = devices;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //new Fonts(activity).setTypeFaceAllTextView((ViewGroup) findViewById(android.R.id.content));

        Window window = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_map_content);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        bindUI();
        setupUI();
    }

    private void bindUI() {
        tvLocationName = findViewById(R.id.tvLocationName);
        tvDistance = findViewById(R.id.tvDistance);
        tvParkingLot = findViewById(R.id.tvParkingLot);
        ivParking = findViewById(R.id.ivParking);
        ivDisable = findViewById(R.id.ivDisable);
        btnNavigation = findViewById(R.id.btnNavigation);
    }

    private void setupUI() {
        gpsTracker = new GPSTracker(activity);
        defaultLocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

        if (!devices.isAvailable()) {
            btnNavigation.setVisibility(View.GONE);
            tvLocationName.setBackgroundResource(R.color.red_transparent);
        } else {
            tvLocationName.setBackgroundResource(R.color.green_transparent);
        }

        if (!devices.isDisable()) {
            ivDisable.setVisibility(View.GONE);
        } else {
            ivDisable.setImageResource(R.drawable.icon_disabled);
        }

        String distance = String.valueOf(Functions.distance(defaultLocation.latitude,
                defaultLocation.longitude,
                devices.getLat(),
                devices.getLng()));

        tvParkingLot.setText(devices.getName());
        tvLocationName.setText(devices.getLocation());
        tvDistance.setText(distance);
        Functions.glideImageCenterCropWithUrl(activity, devices.getImage(), ivParking);

        btnNavigation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNavigation:
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" +
                                latitude + "," +
                                longitude + "&daddr=" +
                                devices.getLat() + "," +
                                devices.getLng()));
                activity.startActivity(intent);
                Toast.makeText(activity, activity.getString(R.string.txt_direction)+ " " + devices.getLocation() + " " + devices.getName(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
