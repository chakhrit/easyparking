package com.chakhrit.eazyparking.fragment;


import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.activity.MainActivity;
import com.chakhrit.eazyparking.custom.AbstractFragment;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.custom.GPSTracker;
import com.chakhrit.eazyparking.dialog.MapWindowDialog;
import com.chakhrit.eazyparking.model.Devices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends AbstractFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, CompoundButton.OnCheckedChangeListener {

    private GoogleMap mMap;
    private GPSTracker gpsTracker;
    private LatLng defaultLocation;

    private SupportMapFragment mapFragment;
    private ToggleButton tgbTraffic;

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private Map<String, Devices> stringDevicesMap;

    @Override
    protected int setContentView() {
        return R.layout.fragment_map;
    }

    @Override
    protected void bindUI(View v) {
        ((MainActivity) activity).menuTitle(getString(R.string.txt_map));
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        tgbTraffic = v.findViewById(R.id.tgbTraffic);
    }

    @Override
    protected void setupUI() {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Devices");

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        getMyLocation();
        fetchMapDataMapData();
        mMap.setOnMarkerClickListener(this);
        tgbTraffic.setOnCheckedChangeListener(this);
    }

    private void fetchMapDataMapData() {
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                generateMarker((Map<String, Object>) dataSnapshot.getValue());
                putMarkers();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Functions.setAlertDialogWithOk(databaseError.getMessage().toString(), activity);
            }
        });
    }

    private void generateMarker(Map<String, Object> value) {
        stringDevicesMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : value.entrySet()) {
            Log.i("entry", entry.getKey());
            /*            Log.i("entry", entry.getValue().toString());*/

            String parkingName = ((Map) entry.getValue()).get("name").toString();
            String locationName = ((Map) entry.getValue()).get("location").toString();
            String imgUrl = ((Map) entry.getValue()).get("image").toString();
            boolean available = (boolean) ((Map) entry.getValue()).get("available");
            boolean disable = (boolean) ((Map) entry.getValue()).get("disable");
            double lat = (double) ((Map) entry.getValue()).get("lat");
            double lng = (double) ((Map) entry.getValue()).get("lng");

/*            Log.i("checkValue", locationName);
            Log.i("checkValue", imgUrl);
            Log.i("checkValue", String.valueOf(available));
            Log.i("checkValue", String.valueOf(lat));
            Log.i("checkValue", String.valueOf(lng));*/

            Devices devices = new Devices(parkingName, locationName, available, disable, lat, lng, imgUrl);
            stringDevicesMap.put(parkingName, devices);
        }
    }


    private void putMarkers() {
        BitmapDescriptor redIcon = BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_red);
        BitmapDescriptor greenIcon = BitmapDescriptorFactory.fromResource(R.drawable.icon_marker_green);
        gpsTracker = new GPSTracker(activity);
        for (Map.Entry<String, Devices> entry : stringDevicesMap.entrySet()) {
            LatLng latLng = new LatLng(entry.getValue().getLat(), entry.getValue().getLng());
            if (entry.getValue().isAvailable()) {
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(greenIcon)
                        .title(entry.getKey()));
            } else {
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .icon(redIcon)
                        .title(entry.getKey()));
            }
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void getMyLocation() {
        gpsTracker = new GPSTracker(activity);

        if (gpsTracker.getIsGPSTrackingEnabled()) {
            defaultLocation = new LatLng(7.893958, 98.352689);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 17));
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Devices devices = stringDevicesMap.get(marker.getTitle());
        final LatLng destination = new LatLng(devices.getLat(), devices.getLng());

        /*GoogleDirection.withServerKey("AIzaSyAJLIoM0u8NELjCSVzp_BMZxMVIn5o6DoE")
                .from(defaultLocation)
                .to(destination)
                .unit(Unit.METRIC)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        Route route = direction.getRouteList().get(0);
                        ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
                        PolylineOptions polylineOptions = DirectionConverter.createPolyline(activity, directionPositionList, 5, Color.RED);
                        mMap.addPolyline(polylineOptions);
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {

                    }
                });*/
        MapWindowDialog mapWindowDialog = new MapWindowDialog(activity, devices,
                gpsTracker.getLatitude(), gpsTracker.getLongitude());
        mapWindowDialog.show();

        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mMap.setTrafficEnabled(true);
        } else {
            mMap.setTrafficEnabled(false);
        }
    }
}