package com.chakhrit.eazyparking.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.activity.MainActivity;
import com.chakhrit.eazyparking.adapter.CustomFilterAdapter;
import com.chakhrit.eazyparking.adapter.FirebaseLocationAdapter;
import com.chakhrit.eazyparking.adapter.FirebaseLocationViewHolder;
import com.chakhrit.eazyparking.custom.AbstractFragment;
import com.chakhrit.eazyparking.custom.GPSTracker;
import com.chakhrit.eazyparking.dialog.MapWindowDialog;
import com.chakhrit.eazyparking.model.Devices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends AbstractFragment implements
        View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rvMap;
    private CardView filterBar;
    private ImageView ivClearFilter;
    private Spinner spinner_sort;
    private TextView tvTitleList;
    private SwipeRefreshLayout swipeRefreshLayout;

    private FirebaseLocationAdapter firebaseLocationAdapter;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("Devices");
    private Query query;

    private LatLng defaultLocation;
    private GPSTracker gpsTracker;

    private String[] sortBy = {"Default", "Filter by available parking", "Filter by disable person", "Building1", "Building5"};

    @Override
    protected int setContentView() {
        return R.layout.fragment_search;
    }

    @Override
    protected void bindUI(View v) {
        ((MainActivity) activity).menuTitle(getString(R.string.txt_filter));
        rvMap = v.findViewById(R.id.rvMap);
        filterBar = v.findViewById(R.id.filter_bar);
        spinner_sort = v.findViewById(R.id.spinner_sort);
        ivClearFilter = v.findViewById(R.id.ivClearFilter);
        tvTitleList = v.findViewById(R.id.tvTitleList);
        swipeRefreshLayout = v.findViewById(R.id.swipeRefreshLayout);
    }

    @Override
    protected void setupUI() {
        gpsTracker = new GPSTracker(activity);
        defaultLocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());

        setSpinnerAdapter();

        ((MainActivity) activity).setClickedRight(this);
        filterBar.setOnClickListener(this);
        ivClearFilter.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setSpinnerAdapter() {
        rvMap.setLayoutManager(new LinearLayoutManager(activity));

        CustomFilterAdapter customFilterAdapter = new CustomFilterAdapter(activity, sortBy);
        spinner_sort.setAdapter(customFilterAdapter);
        spinner_sort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvTitleList.setText(sortBy[position]);
                String sortBy = tvTitleList.getText().toString();
                progressDialog.show();
                if (sortBy.equals("Default")) {
                    query = myRef.orderByChild("name");
                    setFireBaseAdapter(query);
                } else if (sortBy.equals("Filter by available parking")) {
                    query = myRef.orderByChild("available").equalTo(true);
                    setFireBaseAdapter(query);
                } else if (sortBy.equals("Filter by disable person")) {
                    query = myRef.orderByChild("disable").equalTo(true);
                    setFireBaseAdapter(query);
                }else if (sortBy.equals("Building1")) {
                    query = myRef.orderByChild("location").equalTo("Building1");
                    setFireBaseAdapter(query);
                }else if (sortBy.equals("Building5")) {
                    query = myRef.orderByChild("location").equalTo("Building5");
                    setFireBaseAdapter(query);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setFireBaseAdapter(Query query) {
        progressDialog.dismiss();
        firebaseLocationAdapter = new FirebaseLocationAdapter(Devices.class,
                R.layout.map_item,
                FirebaseLocationViewHolder.class,
                query,
                activity);
        rvMap.setAdapter(firebaseLocationAdapter);
        firebaseLocationAdapter.setOnLocationClickListener(new FirebaseLocationAdapter.OnLocationClickListener() {
            @Override
            public void setOnLocationClick(Devices devices) {
                MapWindowDialog mapWindowDialog = new MapWindowDialog(activity, devices, defaultLocation.latitude, defaultLocation.longitude);
                mapWindowDialog.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_bar:
                spinner_sort.performClick();
                break;
            case R.id.ivClearFilter:
                spinner_sort.setSelection(0);
                break;
        }
    }

    @Override
    public void onRefresh() {
        spinner_sort.setSelection(0);
        swipeRefreshLayout.setRefreshing(false);
    }
}
