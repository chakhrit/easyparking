package com.chakhrit.eazyparking.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chakhrit.eazyparking.R;
import com.chakhrit.eazyparking.activity.MainActivity;
import com.chakhrit.eazyparking.activity.SettingActivity;
import com.chakhrit.eazyparking.custom.AbstractFragment;
import com.chakhrit.eazyparking.custom.Functions;
import com.chakhrit.eazyparking.dialog.ChangePasswordDialog;
import com.chakhrit.eazyparking.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;


public class UserInformationFragment extends AbstractFragment implements View.OnClickListener {
    private final int RESULT_OK = 0;
    private final int IMG_REQUEST = 1;

    private Intent intent;
    private Uri imgUri;

    private TextView tvFullName, tvEmail, tvTel;
    private ImageView ivProfile;
    private Button btnChangePassword;

    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference mDatabase;
    private StorageReference mStorageReference;

    @Override
    protected int setContentView() {
        return R.layout.fragment_user_information;
    }

    @Override
    protected void bindUI(View v) {
        ((MainActivity) activity).menuTitleAndRight(getString(R.string.txt_detail), R.drawable.ic_settings_white_24dp);
        tvFullName = v.findViewById(R.id.tvFullName);
        tvEmail = v.findViewById(R.id.tvEmail);
        tvTel = v.findViewById(R.id.tvTel);
        ivProfile = v.findViewById(R.id.ivProfile);
        btnChangePassword = v.findViewById(R.id.btnChangePassword);
    }

    @Override
    protected void setupUI() {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        setInformation();
        setProfileImage();

        ((MainActivity) activity).setClickedRight(this);
        btnChangePassword.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMG_REQUEST:
                try {
                    imgUri = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), imgUri);
                    ivProfile.setImageBitmap(bitmap);
                    uploadProfileImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void setInformation() {
        progressDialog.show();

        String userId = user.getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                tvFullName.setText(user.firstName + " " + user.lastName);
                tvEmail.setText(user.email);
                tvTel.setText(user.tel);
                progressDialog.cancel();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Functions.setAlertDialogWithOk(databaseError.getMessage().toString(), activity);
            }
        });
    }

    private void setProfileImage() {
        Functions.glideImageCircle(activity, R.drawable.icon_profile, ivProfile);
    }

    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, IMG_REQUEST);
        } else {
            intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, IMG_REQUEST);
        }
    }

    private void uploadProfileImage() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivRight:
                intent = new Intent(activity, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.btnChangePassword:
                ChangePasswordDialog changePasswordDialog = new ChangePasswordDialog(activity);
                changePasswordDialog.setCanceledOnTouchOutside(true);
                changePasswordDialog.show();
                break;
            case R.id.ivProfile:
                //checkPermission();
                break;
        }
    }
}
