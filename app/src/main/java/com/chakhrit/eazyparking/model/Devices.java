package com.chakhrit.eazyparking.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by chakhrit on 8/3/2018 AD.
 */
public class Devices implements ClusterItem{

    public static final String SORT_BY_NAME = "name";
    public static final String SORT_BY_AVAILABLE = "available";

    private String name;
    private String location;
    private boolean available;
    private boolean disable;
    private int distance;
    private String image;
    private double lat;
    private double lng;

    public Devices() {
    }

    public Devices(String name, String location, boolean available, boolean disable, double lat, double lng, String image) {
        this.available = available;
        this.disable = disable;
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.image = image;
        this.location = location;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public LatLng getPosition() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
