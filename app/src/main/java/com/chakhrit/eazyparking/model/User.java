package com.chakhrit.eazyparking.model;

import android.net.Uri;

/**
 * Created by chakhrit on 7/3/2018 AD.
 */

public class User {


    /**
     * email : test@gmail.com
     * firstName : test
     * lastName : test
     * tel : 0852580088
     */

    public String email;
    public String firstName;
    public String lastName;
    public String tel;

    public User() {

    }

    public User(String email, String firstName, String lastName, String tel) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
